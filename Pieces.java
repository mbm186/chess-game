package gamePieces; 
import java.util.function.BiFunction;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */

public abstract class Pieces {
	
// we need to know the row, column, color, piece type and whether or not the piece has moved for each chess piece on the board	
	
	private int row;
	private int column;
	private PieceColor color;
	private PieceType piece;
	private boolean moved;
	
//we will now instantiate these values below, constructor 
	
	public Pieces(int row, int column, PieceType piece, PieceColor color){
		this.piece = piece;
		this.row = row;
		this.column = column;
		this.color = color;
		this.moved = false;
	}
	
	
// first thing we need to check is, will the piece try to move outside the 7 rows/columns, if it does, it is not possible 
// for the piece to move and thus, we will apply !canMove 
	
	public void movePiece(int row, int column, BiFunction<Integer, Integer, Boolean> canMove) 
			throws IllegalMoveException {
		if(row < 0 || row > 7 || column < 0 || column > 7 ||
				!canMove.apply(row, column)){
			throw new IllegalMoveException();
		}
		if(this.row == row && this.column == column){
			throw new IllegalMoveException();
		}
		this.row = row;
		this.column = column;
		this.moved = true;
	}
	
	
	public void isMoveLegal(int row, int col) 
			throws IllegalMoveException {
		if (row >= 0 && row <= 7 && col >= 0 && col <= 7)
			return;
		throw new IllegalMoveException();
	}
	
	//This method is for the move taking place during the game, we need to again verify if the actual move the player
	// is making is within the board and then set the original to the temp, in other words, the new position moved to
	// needs to become the actual position
	
	public void actualMove(int temprow, int tempcolumn, boolean hasMoved) 
			throws IllegalMoveException {
		if(temprow < 0 || temprow > 7 || column < 0 || column > 7){
			throw new IllegalMoveException();
		}
		this.row = temprow;
		this.column = tempcolumn;
		this.moved = hasMoved;
	}
	
	// setters and getters needed 
	
	public abstract void move(int row, int col) throws IllegalMoveException;

	public int getRow(){
		return this.row;
	}
	
	public int getCol(){
		return this.column;
	}

	public PieceColor getColor(){
		return this.color;
	}
	
	public PieceType getPieceType() {
		return piece;
	}
	
	public boolean hasMoved(){
		return this.moved;
	}
	
	public void setMoved() {
		moved = true;
	}
	
}


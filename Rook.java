package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
public class Rook extends Pieces {
	//the color, row and column are needed and we must call super because of the constructor rule
	public Rook(int row, int column, PieceColor color){
		super(row, column, PieceType.ROOK, color);
	}
    //check if the moves of a rook are legal
	public void isMoveLegal(int row, int col) throws IllegalMoveException {
		super.isMoveLegal(row, col);
		if(row != getRow() && col != getCol()){
			throw new IllegalMoveException();
		}
	}
	
	@Override
	public void move(int row, int column) throws IllegalMoveException {
		// 
		
		super.movePiece(row, column, (r, c) -> {
			if(r != getRow() && c != getCol()){
				return false;
			}
			return true;		});
	}
	// this returns the string of Rook players
public String toString() {
	char colorCharr;
	if (getColor() == PieceColor.White) {
		colorCharr = 'w';		}
	else {			colorCharr = 'b';		}
	return colorCharr + "R";
}

}

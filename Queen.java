package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
public class Queen extends Pieces{
	//the color, row and column are needed and we must call super because of the constructor rule
	public Queen(int row, int col, PieceColor color){
		super(row, col, PieceType.QUEEN, color);
	}
	//calls bifunction in pieces and check legal moves
	public void isMoveLegal(int row, int column) throws IllegalMoveException {
		super.isMoveLegal(row, column);
		if(row != getRow() && column != getCol()){
			int dr = row - getRow();
			if (!(column == getCol() + dr || column == getCol() - dr)) {
				throw new IllegalMoveException();
			}
		}
	}

	@Override
	public void move(int row, int column) throws IllegalMoveException {
		// TODO Auto-generated method stub
		super.movePiece(row, column, (r, c) -> {
			if(r != getRow() && c != getCol()){
				int dr = row - getRow();
				return (column == getCol() + dr || column == getCol() - dr);
			}
			return true;
		});
	}
	// this returns the string of Queen players
	public String toString() {
		char colorCharr;
		if (getColor() == PieceColor.White) {	
			colorCharr = 'w';		}
		else {			colorCharr = 'b';		}
		return colorCharr + "Q";
	}
	
}

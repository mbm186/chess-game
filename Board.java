package game;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import gamePieces.*;

public class Board {
	private int[][] private_white;
	private int[][] private_black;
	private Square[][] gameBoard;
	private HashMap<PieceType, Integer> whitePlayersPieces;
	private HashMap<PieceType, Integer> blackPlayersPieces;
	public List<Pieces> piecesList = new ArrayList<Pieces>();
	public static final char[] rowschar = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	public Board() {
		private_white = new int[8][8];
		gameBoard = new Square[8][8];
		whitePlayersPieces = new HashMap<PieceType, Integer>();
		private_black = new int[8][8];
		blackPlayersPieces = new HashMap<PieceType, Integer>();
		//initializes new board sets color integer for making game board 
		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				int color;
				switch (row % 2) {
				case 0:
					switch (col % 2) {
					case 0:
						color = 1;
						break;
					default:
						color = 0;
						break;
					}
					break;
				default:
					color = col % 2 == 0 ? 0 : 1;
					break;
				}
				gameBoard[row][col] = new Square(rowschar[col], row, col, color);
			}
		}
	}

	public void initialize() {
		for (int column = 0; column < 8; column++) {
			gameBoard[1][column].occupy(new Pawn(1, column, PieceColor.Black));
			gameBoard[6][column].occupy(new Pawn(6, column, PieceColor.White));
			if (column != 0 && column != 7) {
				if (column != 1 && column != 6) {
					if (column != 2 && column != 5) {
						if (column != 3) {
							gameBoard[0][column].occupy(new King(0, column, PieceColor.Black));
							gameBoard[7][column].occupy(new King(7, column, PieceColor.White));
						} else {
							gameBoard[0][column].occupy(new Queen(0, column, PieceColor.Black));
							gameBoard[7][column].occupy(new Queen(7, column, PieceColor.White));
						}
					} else {
						gameBoard[0][column].occupy(new Bishop(0, column, PieceColor.Black));
						gameBoard[7][column].occupy(new Bishop(7, column, PieceColor.White));
					}
				} else {
					gameBoard[0][column].occupy(new Knight(0, column, PieceColor.Black));
					gameBoard[7][column].occupy(new Knight(7, column, PieceColor.White));
				}
			} else {
				gameBoard[0][column].occupy(new Rook(0, column, PieceColor.Black));
				gameBoard[7][column].occupy(new Rook(7, column, PieceColor.White));
			}
		}
	}
	public String[][] board = new String[8][8];	

	public Square getSquare(int row, int column) {
		return gameBoard[row][column];
	}
	
	public HashMap<PieceType, Integer> getBlackCounts() {
		return blackPlayersPieces;
	}
	
	public HashMap<PieceType, Integer> getWhiteCounts() {
		return whitePlayersPieces;
	}
	

	public int[][] getCoveredWhite() {
		return private_white;
	}

		public void initializePieceCounts() {
			whitePlayersPieces.put(PieceType.PAWN, 8);
			whitePlayersPieces.put(PieceType.QUEEN, 1);
			whitePlayersPieces.put(PieceType.ROOK, 2);
			whitePlayersPieces.put(PieceType.KNIGHT, 2);
			whitePlayersPieces.put(PieceType.BISHOP, 2);
			blackPlayersPieces.put(PieceType.PAWN, 8);
			blackPlayersPieces.put(PieceType.QUEEN, 1);
			blackPlayersPieces.put(PieceType.ROOK, 2);
			blackPlayersPieces.put(PieceType.KNIGHT, 2);
			blackPlayersPieces.put(PieceType.BISHOP, 2);
		}	
	
		public void resetCoveredSquares() {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					private_white[i][j] = 0;
					private_black[i][j] = 0;
				}
			}
		}
	

		public void printBoard() {
			for (int row = 0; row < 8; row++) {
				for (int col = 0; col < 8; col++) {
					System.out.print(gameBoard[row][col].display() + " ");
				}
				System.out.println(8 - row);
			}
			for (int print = 0; print < 8; print++) {
				System.out.print(" " + rowschar[print] + " ");
			}
			System.out.println();
		}
		
		
		
	
	

	

	public boolean draw() {
		//check-mate is possible if there are queens and/or rooks and/or pawns on the board
		if (whitePlayersPieces.get(PieceType.QUEEN) <= 0 && blackPlayersPieces.get(PieceType.QUEEN) <= 0 && whitePlayersPieces.get(PieceType.ROOK) <= 0 && blackPlayersPieces.get(PieceType.ROOK) <= 0 && whitePlayersPieces.get(PieceType.PAWN) <= 0 && blackPlayersPieces.get(PieceType.PAWN) <= 0) {
			//can check-mate with 2+ bishops
			if (whitePlayersPieces.get(PieceType.BISHOP) >= 2 || blackPlayersPieces.get(PieceType.BISHOP) >= 2) {
				return false;
			}
			return true;
		} else {
return false;
}
	}

		
		
		public int[][] getCoveredBlack() {
			return private_black;
		}
		
	
}


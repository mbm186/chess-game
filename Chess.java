package chess;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */

import java.util.Scanner;
import game.Board;
import game.TestBoard;
import gamePieces.IllegalMoveException;
import gamePieces.PieceColor;


public class Chess{
	
	static Boolean resign= false;
	static Boolean player=true;
	static Scanner input =new Scanner(System.in);
	static PieceColor colorOfgame;
	 public static final int KNIGHT = 0;
	    public static final int BISHOP = 1;
	    public static final int ROOK   = 2;
	    public static final int QUEEN  = 3;
	    public static final int KING   = 4;
	    public static final int PAWN   = 5;
	    public static final int BLACK  = 0;
	    public static final int WHITE  = 1;
	    public int turn;

	
	
	
 
   public static void main(String[] args)throws  Exception, IllegalMoveException {
		   
		Board newGame= new Board();
		newGame.initialize();
		newGame.initializePieceCounts();
		newGame.printBoard();
	
		 @SuppressWarnings("unused")
		int Counter =0; 
		
		 
		 

		while((TestBoard.getLastPawnMoveOrCapture()<50)&&!newGame.draw()&&!TestBoard.isEnd()){
			Counter++;
		
			if(!player) { colorOfgame=PieceColor.Black;
			System.out.print("\n Black's Turn ");
				
			} else {
				colorOfgame=PieceColor.White;
				System.out.print("\n White's Turn ");}//endif white 
			
			try{
				
				TestBoard.updateProtectedSquares(newGame);
				
				String gameMove=input.nextLine();
				
				if(gameMove.equalsIgnoreCase("resign")){
					switch (colorOfgame) {
					case Black:
						System.out.print("White wins ");
						break;
					default:
						System.out.print("Black wins ");
						break;
					}
					break;
				} 
				
				gameMove.length();
				if (gameMove.length() == 7) {
					TestBoard.promotePawn(gameMove,newGame,colorOfgame);
				} else {
					TestBoard.movePiece(gameMove,newGame,colorOfgame);
				}	
			}
			//we need to catch any exceptions
			catch(IllegalMoveException e){
				Counter--;
				System.out.println("Illegal move, try again");
				continue;}
			catch ( Exception e   ){
				Counter--;
				System.out.println("Incorrect Move. Try Again");
				continue;}
			
			
			System.out.println();
			newGame.printBoard();
			TestBoard.updateProtectedSquares(newGame);
			TestBoard.inCheck(newGame,colorOfgame);
			if (!TestBoard.getCheckStatus()) {
			} else {System.out.println("In check");}
			player = player ? false : true;	
		}
		input.close();
   }

}

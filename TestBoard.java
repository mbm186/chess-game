package game;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
import java.util.ArrayList;
import java.util.List;
import chess.GameMoves;
import game.Board;
import game.Square;
import gamePieces.*;


public class TestBoard extends Exception {
	//keep track of player pieces
	static int lastPawnMoveOrCapture = 0;
	static ArrayList<String> white_moves = new ArrayList<String>();
	static ArrayList<String> black_moves = new ArrayList<String>();
	
	
	public List<Pieces>  blackPlayer=new ArrayList<Pieces>();
	public List<Pieces>  whitePlayer=new ArrayList<Pieces>();
	static boolean check = false; 
	static boolean end = false;  
	
	public TestBoard(){
		
	}
	
	
	/**
	 * method checks for the opposite color by utilizing the arguments: Board and PieceColor. This will lead to checking if there are any legal moves 
	 * @param currentBoard  board 
	 * @param checkColor takes in color param to check 
	 * @return does trial move with current board  returns boolean 
	 */
	public static boolean hasLegalMoves(Board currentBoard, PieceColor checkColor) {
		PieceColor opposite;
		switch (checkColor) {
		case White:
			opposite = PieceColor.Black;
			break;
		default:
			opposite = PieceColor.White;
			break;
		}
		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				Square temp = currentBoard.getSquare(row, col);
				Pieces playertemp = temp.getPiece();
				if (playertemp == null || playertemp.getColor() != opposite)
					continue;
				for (int newrow = 0; newrow < 8; newrow++) {
					for (int newcol = 0; newcol < 8; newcol++) {
						if (newrow == row && newcol == col)
							continue;
						//trial checker
						try {
							if(trialMove(row, col, newrow, newcol, currentBoard, opposite)){
								return true;
							}
						} catch (Exception  e) {
							continue;	} finally {
							}	}	}	}
		}
		end = true;
		return false;
	}
	
	public static void inCheck(Board current, PieceColor playersColor) {
		for (int row = 0; row < 8; row++) {
			for (int col = 0; col < 8; col++) {
				Square playertemp = current.getSquare(row, col);
				if (playertemp.getPiece() != null && playertemp.getPiece().getPieceType() == PieceType.KING && playertemp.getPiece().getColor() != playersColor) {
					switch (playersColor) {
					case Black:
						//if its black king set boolean to true
						if (current.getCoveredBlack()[row][col] == 1) {
							check = true;
							return;			}
						break;
					default:
						//checks white knight 
						if (current.getCoveredWhite()[row][col] == 1) {
							check = true;
							return;				}
						break;
					}
				}
			}
		}
		check = false;
	}
	
	public static void movePiece(String move, Board board, PieceColor color) 
			throws Exception,IllegalMoveException {
		PieceColor otherTemp;
		switch (color) {
		case White:
			otherTemp = PieceColor.Black;
			break;
		default:
			otherTemp = PieceColor.White;
			break;
		}
		//sets array of rows and columns
		int[] rowsandColumns = GameMoves.convertMove(move);
		if(color == board.getSquare(rowsandColumns[0], rowsandColumns[1]).getPiece().getColor()) {
		} else {
			throw new Exception();
		}
		//calls path blocked method for move of piece 
		isPathBlocked(board, rowsandColumns[0], rowsandColumns[1], rowsandColumns[2], rowsandColumns[3]);
		Square playsquare = board.getSquare(rowsandColumns[0], rowsandColumns[1]);
		Square checkSquare = board.getSquare(rowsandColumns[2], rowsandColumns[3]);
		Pieces currentPiece = playsquare.getPiece();
		Pieces tempPiece = checkSquare.getPiece();
		boolean hasMovedTemp = currentPiece.hasMoved();
		boolean hasMovedTemp2 = false;
		
		if(tempPiece == null) {
} else {
			hasMovedTemp2 = tempPiece.hasMoved();		}
		
		
		switch (currentPiece.getPieceType()) {
		case KING:
			Pieces tempRook;
			switch (currentPiece.getColor()) {
			case White:
				if (playsquare.toString().equals("[7,4]") && (checkSquare.toString().equals("[7,6]") || checkSquare.toString().equals("[7,2]"))) {
					if (!checkSquare.toString().equals("[7,6]")) {
						if (checkSquare.toString().equals("[7,2]")) {
							if (board.getSquare(checkSquare.getRow(), checkSquare.column() + 1).isEmpty()) {
							} else {
								throw new Exception();
							}
							tempRook = board.getSquare(checkSquare.getRow(), checkSquare.column() - 2).getPiece();
							if (!checkCastle(currentPiece, tempRook, board)) {
								throw new IllegalMoveException();
							} else {
								currentPiece.move(checkSquare.getRow(), checkSquare.column());
								board.getSquare(checkSquare.getRow(), checkSquare.column()).occupy(currentPiece);
								playsquare.removePiece();
								board.getSquare(checkSquare.getRow(), playsquare.column() - 1).occupy(new Rook(checkSquare.getRow(), playsquare.column() - 1, currentPiece.getColor()));
								board.getSquare(checkSquare.getRow(), checkSquare.column() - 2).removePiece();
								lastPawnMoveOrCapture++;
								return;
							}
						}
					} else {
						if (!board.getSquare(checkSquare.getRow(), checkSquare.column() - 1).isEmpty()) {
							throw new Exception();
						}
						tempRook = board.getSquare(checkSquare.getRow(), checkSquare.column() + 1).getPiece();
						if (!checkCastle(currentPiece, tempRook, board)) {
							throw new IllegalMoveException();					} else {
							currentPiece.move(checkSquare.getRow(), checkSquare.column());
							board.getSquare(checkSquare.getRow(), checkSquare.column()).occupy(currentPiece);
							playsquare.removePiece();
							board.getSquare(checkSquare.getRow(), playsquare.column() + 1).occupy(new Rook(checkSquare.getRow(), playsquare.column() + 1, currentPiece.getColor()));
							board.getSquare(checkSquare.getRow(), checkSquare.column() + 1).removePiece();
							lastPawnMoveOrCapture++;
							return;
						}					}
				} else {
				}
				break;
			default:
				if (playsquare.toString().equals("[0,4]") && (checkSquare.toString().equals("[0,6]") || checkSquare.toString().equals("[0,2]"))) {
					if (checkSquare.toString().equals("[0,6]")) {
						if (board.getSquare(checkSquare.getRow(), checkSquare.column() - 1).isEmpty()) {
						} else {
							throw new Exception();
						}
						tempRook = board.getSquare(checkSquare.getRow(), checkSquare.column() + 1).getPiece();
						if (!checkCastle(currentPiece, tempRook, board)) {
							throw new IllegalMoveException();
						} else {
							currentPiece.move(checkSquare.getRow(), checkSquare.column());
							currentPiece.setMoved();
							board.getSquare(checkSquare.getRow(), checkSquare.column()).occupy(currentPiece);
							playsquare.removePiece();
							board.getSquare(checkSquare.getRow(), playsquare.column() + 1).occupy(new Rook(checkSquare.getRow(), playsquare.column() + 1, currentPiece.getColor()));
							board.getSquare(checkSquare.getRow(), playsquare.column() + 1).getPiece().setMoved();
							board.getSquare(checkSquare.getRow(), checkSquare.column() + 1).removePiece();
							lastPawnMoveOrCapture++;
							return;
						}
					}
					switch (checkSquare.toString()) {
					case "[0,2]":
						if (!board.getSquare(checkSquare.getRow(), checkSquare.column() + 1).isEmpty()) {
							throw new Exception();
						}
						tempRook = board.getSquare(checkSquare.getRow(), checkSquare.column() - 2).getPiece();
						if (!checkCastle(currentPiece, tempRook, board)) {
							throw new IllegalMoveException();
						} else {
							currentPiece.move(checkSquare.getRow(), checkSquare.column());
							currentPiece.setMoved();
							board.getSquare(checkSquare.getRow(), checkSquare.column()).occupy(currentPiece);
							playsquare.removePiece();
							board.getSquare(checkSquare.getRow(), playsquare.column() - 1).occupy(new Rook(checkSquare.getRow(), playsquare.column() - 1, currentPiece.getColor()));
							board.getSquare(checkSquare.getRow(), playsquare.column() - 1).getPiece().setMoved();
							board.getSquare(checkSquare.getRow(), checkSquare.column() - 2).removePiece();
							lastPawnMoveOrCapture++;
							return;
						}
					}	
				}
				break;
			}
			break;
		default:
			break;
		}
		
		switch (currentPiece.getPieceType()) {
		case PAWN:
			//check for if its capturing)
			if (!pawnCapture(currentPiece, board, checkSquare)) {
				if (currentPiece.getColor() != PieceColor.White) {
					if (checkSquare != board.getSquare(playsquare.getRow() + 1, playsquare.column()) && currentPiece.hasMoved()) {
						if (currentPiece.hasMoved()) {
							if ((!pawnCapture(currentPiece, board, checkSquare) && checkSquare.isEmpty()) || checkSquare.isEmpty() || checkSquare.getPiece().getColor() == PieceColor.Black) {
								throw new IllegalMoveException();
							}
						} else {
							if (checkSquare != board.getSquare(playsquare.getRow() + 2, playsquare.column()) && (checkSquare.isEmpty() || checkSquare.getPiece().getColor() == PieceColor.Black)) {
								throw new IllegalMoveException();
							}
						}
					} else {
					}
					if (checkSquare != board.getSquare(playsquare.getRow() + 1, playsquare.column()) && checkSquare != board.getSquare(playsquare.getRow() + 2, checkSquare.column())) {
					} else {
						Pieces blocked = checkSquare.getPiece();
						if (blocked != null) {
							throw new Exception();
						}
					}
				} else {
					if (checkSquare != board.getSquare(playsquare.getRow() - 1, playsquare.column())) {
						if (!currentPiece.hasMoved()) {
							if (checkSquare != board.getSquare(playsquare.getRow() - 2, playsquare.column()) && (checkSquare.isEmpty() || checkSquare.getPiece().getColor() == PieceColor.White)) {
								throw new IllegalMoveException();
							}
						} else { 
							if ((!pawnCapture(currentPiece, board, checkSquare) && checkSquare.isEmpty()) || checkSquare.isEmpty() || checkSquare.getPiece().getColor() == PieceColor.White) {
								throw new IllegalMoveException();
							}
						}
					}
					if (checkSquare != board.getSquare(playsquare.getRow() - 1, playsquare.column()) && checkSquare != board.getSquare(playsquare.getRow() - 2, checkSquare.column())) {
					} else {
						Pieces blocked = checkSquare.getPiece();
						if (blocked != null) {
							throw new Exception();
						}
					}
				}
			} else {
				Square adj_square;
				if (currentPiece.getColor() != PieceColor.White) {
					adj_square = board.getSquare(checkSquare.getRow() - 1, checkSquare.column());
					board.getWhiteCounts().put(PieceType.PAWN, board.getWhiteCounts().get(PieceType.PAWN) - 1);
				} else {
					adj_square = board.getSquare(checkSquare.getRow() + 1, checkSquare.column());
					board.getBlackCounts().put(PieceType.PAWN, board.getBlackCounts().get(PieceType.PAWN) - 1);
				}
				adj_square.removePiece();
			}
			break;
		case BISHOP:
			break;
		case KING:
			break;
		case KNIGHT:
			break;
		case QUEEN:
			break;
		case ROOK:
			break;
		default:
			break;
		}
		//places capture piece in temp deducts from piece count and adds to lastpawn variable and then removes captured piece 
		Pieces captured = checkSquare.getPiece();
		if (captured == null && currentPiece.getPieceType() != PieceType.PAWN) {
			lastPawnMoveOrCapture++;
		} else {
			if (captured != null) {
				switch (captured.getColor()) {
				case White:
					board.getWhiteCounts().put(captured.getPieceType(), board.getWhiteCounts().get(captured.getPieceType()) - 1);
					break;
				default:
					board.getBlackCounts().put(captured.getPieceType(), board.getBlackCounts().get(captured.getPieceType()) - 1);
					break;
				}
				checkSquare.removePiece();
			}
			lastPawnMoveOrCapture = 0;
		}
		currentPiece.move(checkSquare.getRow(), checkSquare.column());
		updateProtectedSquares(board);
		currentPiece.setMoved();
		checkSquare.occupy(currentPiece);
		playsquare.removePiece();
		switch (currentPiece.getColor()) {
		case White:
			white_moves.add(move);
			break;
		default:
			black_moves.add(move);
			break;
		}
		switch (checkSquare.getPiece().getPieceType()) {
		case PAWN:
			switch (checkSquare.getPiece().getColor()) {
			case White:
				if (checkSquare.getRow() == 0) {
					checkSquare.removePiece();
					checkSquare.occupy(new Queen(checkSquare.getRow(), checkSquare.column(), color));
					board.getWhiteCounts().put(PieceType.QUEEN, board.getWhiteCounts().get(PieceType.QUEEN) + 1);
					board.getWhiteCounts().put(PieceType.PAWN, board.getWhiteCounts().get(PieceType.PAWN) - 1);
				}
				break;
			default:
				switch (checkSquare.getRow()) {
				case 7:
					checkSquare.removePiece();
					checkSquare.occupy(new Queen(checkSquare.getRow(), checkSquare.column(), color));
					board.getBlackCounts().put(PieceType.QUEEN, board.getBlackCounts().get(PieceType.QUEEN) + 1);
					board.getBlackCounts().put(PieceType.PAWN, board.getBlackCounts().get(PieceType.PAWN) - 1);
					break;
				}
				break;
			}
			break;
		default:
			break;
		}
		
		
		//updates all the squares checks for in check the re-updates 
		updateProtectedSquares(board);
		inCheck(board, otherTemp);
		updateProtectedSquares(board);
		switch (color) {
		case White:
			inCheck(board, PieceColor.Black);
			break;
		default:
			inCheck(board, PieceColor.White);
			break;
		}
		if(!check) {
		} else {	
			currentPiece.actualMove(rowsandColumns[0], rowsandColumns[1], hasMovedTemp);
			playsquare.occupy(currentPiece);
			checkSquare.removePiece();
			if(tempPiece != null){
				tempPiece.actualMove(rowsandColumns[2], rowsandColumns[3], hasMovedTemp2);
				checkSquare.occupy(tempPiece);
			}
			throw new Exception();
		}
		if (hasLegalMoves(board, color))
			return;
		updateProtectedSquares(board);
		inCheck(board, color);
		System.out.println();
		board.printBoard();
		System.out.println();
		if(!check) {
			System.out.println("Stalemate");
		} else {
			String col = color == PieceColor.Black ? "Black" : "White";
			System.out.println(col + " wins");
		}
		System.exit(0);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static int getLastPawnMoveOrCapture() {
		return lastPawnMoveOrCapture;
	}
	
	
	public static void updateProtectedSquares(Board currentBoard) {
		currentBoard.resetCoveredSquares();
		for (int row1 = 0; row1 < 8; row1++) {
			for (int col1 = 0; col1 < 8; col1++) {
				Pieces tempPlayer = currentBoard.getSquare(row1, col1).getPiece();
				if (tempPlayer == null)
					continue;
				switch (tempPlayer.getPieceType()) {
				case PAWN:
					int column = tempPlayer.getCol();
					int row = tempPlayer.getRow();
					if (row != 0 && row != 7) {
						switch (column) {
						case 0:
						case 7:
							if (column != 0) {
								switch (column) {
								case 7:
									if (tempPlayer.getColor() == PieceColor.White) {
										currentBoard.getCoveredWhite()[row-1][column-1] = 1;
									}
									else {
										currentBoard.getCoveredBlack()[row+1][column-1] = 1;
									}
									break;
								}
							}
							else {
								switch (tempPlayer.getColor()) {
								case White:
									currentBoard.getCoveredWhite()[row-1][column+1] = 1;
									break;
								default:
									currentBoard.getCoveredBlack()[row+1][column+1] = 1;
									break;
								}
							}
							break;
						default:
							if (tempPlayer.getColor() != PieceColor.White) {
								currentBoard.getCoveredBlack()[row+1][column+1] = 1;
								currentBoard.getCoveredBlack()[row+1][column-1] = 1;
							} else {
								currentBoard.getCoveredWhite()[row-1][column+1] = 1;
								currentBoard.getCoveredWhite()[row-1][column-1] = 1;
							}
							break;
						}
					}
					break;
				default:
					for (int rowloop = 0; rowloop < 8; rowloop++) {
						for (int colloop = 0; colloop < 8; colloop++) {
							if (rowloop == row1 && colloop == col1)
								continue;
							try {
								tempPlayer.isMoveLegal(rowloop, colloop);
								isPathBlocked(currentBoard, row1, col1, rowloop, colloop);
							} catch (Exception  e) {
								continue;
							}
							switch (tempPlayer.getColor()) {
							case White:
								currentBoard.getCoveredWhite()[rowloop][colloop] = 1;
								break;
							default:
								currentBoard.getCoveredBlack()[rowloop][colloop] = 1;
								break;
							}
						}
					} 
					break;
				}
			}
		}
	}
	
	
	/**
	 * Mutator void method that sets the boolean to true if ended 
	 */
	public static void setEnd() {
		end = true;
	}
	
	private static boolean checkCastle(Pieces piece1, Pieces piece2, Board currentBoard) throws Exception {
		if (piece1.getPieceType() == PieceType.KING && (piece2 != null && piece2.getPieceType() == PieceType.ROOK)) {
			if (piece1.getColor() == piece2.getColor()) {
				if (!piece1.hasMoved() && !piece2.hasMoved()) {
					if (!check) {
						Square right_side = currentBoard.getSquare(piece1.getRow(), piece1.getCol() + 2);
						Square left_side = currentBoard.getSquare(piece1.getRow(), piece1.getCol() - 2);
						if (!(right_side.isEmpty() && currentBoard.getSquare(right_side.getRow(), right_side.column() - 1).isEmpty()) &&
								!(left_side.isEmpty() && currentBoard.getSquare(left_side.getRow(), left_side.column() + 1).isEmpty())) {
							return false;
						}
						//can't castle through or into check
						//method checks movement to see if in check or not returns false if move puts in check 
						else {
							boolean safeOnRight = true;
							boolean safeOnLeft = true;
							if (piece1.getColor() != PieceColor.White) {
								if (currentBoard.getCoveredWhite()[right_side.getRow()][right_side.column()] == 1 ||
										currentBoard.getCoveredWhite()[right_side.getRow()][right_side.column() - 1] == 1) {
									safeOnRight = false;
								}
								if (currentBoard.getCoveredWhite()[left_side.getRow()][left_side.column()] == 1 ||
										currentBoard.getCoveredWhite()[left_side.getRow()][left_side.column() + 1] == 1) {
									safeOnLeft = false;
								}
							} else {
								if (currentBoard.getCoveredBlack()[right_side.getRow()][right_side.column()] != 1 && currentBoard.getCoveredBlack()[right_side.getRow()][right_side.column() - 1] != 1) {
								} else {
safeOnRight = false;
}
								if (currentBoard.getCoveredBlack()[left_side.getRow()][left_side.column()] != 1 && currentBoard.getCoveredBlack()[left_side.getRow()][left_side.column() + 1] != 1) {
								} else {
safeOnLeft = false;
}
							}
							if (safeOnRight || safeOnLeft) {
} else {
								return false;				}
						}
						return true;		} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}	}
	
	
	public static void promotePawn(String move, Board board, PieceColor color) throws Exception,IllegalMoveException {
		if(move.charAt(5) == ' ') {
		} else {
			throw new IllegalMoveException();
		}
		String chamgetemp = ("" + move.charAt(6)).toLowerCase(); //character representation of the piece to promote to
		int[] rowsAndColumns =  GameMoves.convertMove(move.substring(0, 5));
		Square start = board.getSquare(rowsAndColumns[0], rowsAndColumns[1]), end = board.getSquare(rowsAndColumns[2], rowsAndColumns[3]);
		if(color == start.getPiece().getColor()) {
		} else {
			throw new Exception();
		}
		isPathBlocked(board, rowsAndColumns[0], rowsAndColumns[1], rowsAndColumns[2], rowsAndColumns[3]);
		//move piece accordingly
		System.out.println(move);
		movePiece(move.substring(0, 5), board, color);
		Pieces tempPiece;
		end.removePiece();
		
		if (!"n".equals(chamgetemp)) {
			if (!"q".equals(chamgetemp)) {
				if (!"b".equals(chamgetemp)) {
					throw new IllegalMoveException();
				} else {
					tempPiece = new Bishop(rowsAndColumns[2], rowsAndColumns[3], color);
				}
			} else {
				tempPiece = new Queen(rowsAndColumns[2], rowsAndColumns[3], color);
			}
		} else {
			tempPiece = new Knight(rowsAndColumns[2], rowsAndColumns[3], color);
		}
		
		end.occupy(tempPiece);
		lastPawnMoveOrCapture = 0;
		if (color != PieceColor.White) {
			board.getBlackCounts().put(tempPiece.getPieceType(), board.getBlackCounts().get(tempPiece.getPieceType()) + 1);
			board.getBlackCounts().put(PieceType.PAWN, board.getBlackCounts().get(PieceType.PAWN) - 1);
		} else {
			board.getWhiteCounts().put(tempPiece.getPieceType(), board.getWhiteCounts().get(tempPiece.getPieceType()) + 1);
			board.getWhiteCounts().put(PieceType.PAWN, board.getWhiteCounts().get(PieceType.PAWN) - 1);
		}
		updateProtectedSquares(board);
	}


	public int positionLettertoInt(String letter) {
		if (!"a".equals(letter)) {
			if (!"b".equals(letter)) {
				if (!"c".equals(letter)) {
					if (!"d".equals(letter)) {
						if (!"e".equals(letter)) {
							if (!"f".equals(letter)) {
								if (!"g".equals(letter)) {
									if ("h".equals(letter)) {
										return 7;
									}
								} else {
									return 6;
								}
							} else {
								return 5;
							}
						} else {
							return 4;
						}
					} else {
						return 3;
					}
				} else {
					return 2;
				}
			} else {
				return 1;
			}
		} else {
			return 0;
		}
		return -1;

	}
	
	public String positionInttoLetter(int number) {
		if (number != 0) {
			if (number != 1) {
				if (number != 2) {
					if (number != 3) {
						if (number != 4) {
							if (number != 5) {
								if (number != 6) {
									if (number == 7) {
										return "h";
									}
								} else {
									return "g";
								}
							} else {
								return "f";
							}
						} else {
							return "e";
						}
					} else {
						return "d";
					}
				} else {
					return "c";
				}
			} else {
				return "b";
			}
		} else {
			return "a";
		}
		return "x";

	}

	private static void isPathBlocked(Board board, int fromRow, int fromcolumn, int toRow, int toColumn) throws Exception {
		
		Square start = board.getSquare(fromRow, fromcolumn);
		Square dest = board.getSquare(toRow, toColumn);
		Pieces player = start.getPiece();
		//check path
		PieceColor current = board.getSquare(fromRow, fromcolumn).getPiece().getColor();
		Square pathSquare;
		int queen;
		int row, column;
		PieceType pieceType = player.getPieceType();
		if (pieceType != PieceType.QUEEN) {
			if (pieceType != PieceType.BISHOP) {
				if (pieceType != PieceType.ROOK) {
					if (pieceType != PieceType.PAWN) {
					} else {
						if (!player.hasMoved()) {
							switch (player.getColor()) {
							case Black:
								pathSquare = board.getSquare(fromRow + 1, fromcolumn);
								break;
							default:
								pathSquare = board.getSquare(fromRow - 1, fromcolumn);
								break;
							}
							if (!pathSquare.isEmpty()) {
								throw new Exception();
							}
						}
					}
				} else {
					int a;
					//if columns are the same it checks 
					if (fromcolumn != toColumn) {
						if (fromRow == toRow){
							if (fromcolumn < toColumn) {
								for (a = fromcolumn + 1; a < toColumn; a++) {
									pathSquare = board.getSquare(fromRow, a);
									if (pathSquare.isEmpty())
										continue;
									throw new Exception();
								}
							}
							else {
								for (a = fromcolumn - 1; a > toColumn; a--) {
									pathSquare = board.getSquare(fromRow, a);
									if (!pathSquare.isEmpty()) {
										throw new Exception();	}		}
							}
						}
					} else {
						if (fromRow >= toRow) {
							for (a = fromRow - 1; a > toRow; a--) {
								pathSquare = board.getSquare(a, fromcolumn);
								if (!pathSquare.isEmpty()) {
									throw new Exception();	}			}
						} else {
							for (a = fromRow + 1; a < toRow; a++) {
								pathSquare = board.getSquare(a, fromcolumn);
								if (!pathSquare.isEmpty()) {
									throw new Exception();	}
							}
						}
					}
				}
			} else {
				if (fromRow <= toRow) {
					row = fromRow + 1;
					
					if (fromcolumn <= toColumn) {
						column = fromcolumn + 1;
						while (row < toRow && column < toColumn) {
							pathSquare = board.getSquare(row, column);
							if (pathSquare.isEmpty()) {
							} else {
								throw new Exception();
							}
							row++;
							column++;					}
					} else {
						column = fromcolumn - 1;
						while (row < toRow && column > toColumn) {
							pathSquare = board.getSquare(row, column);
							if (pathSquare.isEmpty()) {
							} else {
								throw new Exception();
							}
							row++;
							column--;
						}
					}
				} else {
					
					row = fromRow - 1;
					if (fromcolumn <= toColumn) {
						column = fromcolumn + 1;
						while (row > toRow && column < toColumn) {
							pathSquare = board.getSquare(row, column);
							if (pathSquare.isEmpty()) {
							} else {
								throw new Exception();
							}
							row--;
							column++;
						}
					} else {
						column = fromcolumn - 1;
						while (row > toRow && column > toColumn) {
							pathSquare = board.getSquare(row, column);
							if (pathSquare.isEmpty()) {
							} else {
								throw new Exception();
							}
							row--;
							column--;
						}
					}
				}
			}
		} else {
			
			//if columns  are the same just same as rook case above 
			if (fromcolumn != toColumn) {
				if (fromRow == toRow){
					if (fromcolumn >= toColumn) {
						for (queen = fromcolumn - 1; queen > toColumn; queen--) {
							pathSquare = board.getSquare(fromRow, queen);
							if (pathSquare.isEmpty())
								continue;
							throw new Exception();		}
					} else {
						for (queen = fromcolumn + 1; queen < toColumn; queen++) {
							pathSquare = board.getSquare(fromRow, queen);
							if (pathSquare.isEmpty())
								continue;
							throw new Exception();
						}
					}
				}
			} else {
				if (fromRow >= toRow) {
					for (queen = fromRow - 1; queen > toRow; queen--) {
						pathSquare = board.getSquare(queen, fromcolumn);
						if (!pathSquare.isEmpty()) {
							throw new Exception();	}	}
				} else {
					for (queen = fromRow + 1; queen < toRow; queen++) {
						pathSquare = board.getSquare(queen, fromcolumn);
						if (!pathSquare.isEmpty()) {
							throw new Exception();
						}
					}
				}
			}
			if (fromRow <= toRow) {
				row = fromRow + 1;
				
				if (fromcolumn <= toColumn) {
					column = fromcolumn + 1;
					while (row < toRow && column < toColumn) {
						pathSquare = board.getSquare(row, column);
						if (pathSquare.isEmpty()) {
						} else {
							throw new Exception();
						}
						row++;
						column++;
					}
				} else {
					column = fromcolumn - 1;
					while (row < toRow && column > toColumn) {
						pathSquare = board.getSquare(row, column);
						if (!pathSquare.isEmpty()) {
							throw new Exception();
						}
						row++;
						column--;
					}
				}
			} else {
			
				row = fromRow - 1;
								if (fromcolumn > toColumn) {
					column = fromcolumn - 1;
					while (row > toRow && column > toColumn) {
						pathSquare = board.getSquare(row, column);
						if (pathSquare.isEmpty()) {
						} else {
							throw new Exception();
						}
						row--;
						column--;
					}
				}
				
				else {
					column = fromcolumn + 1;
					while (row > toRow && column < toColumn) {
						pathSquare = board.getSquare(row, column);
						if (pathSquare.isEmpty()) {
						} else {
							throw new Exception();
						}
						row--;
						column++;
					}
				}
			}
		}
		if (dest.getPiece() == null)
			return;
		if (current != dest.getPiece().getColor())
			return;
		if (start.getPiece().getPieceType() != PieceType.PAWN) {
			switch (current) {
			case White:
				board.getCoveredWhite()[toRow][toColumn] = 1;
				break;
			default:
				board.getCoveredBlack()[toRow][toColumn] = 1;
				break;
			}
		} else {
		}
		throw new Exception();
	}
	
	
	
	public void capture (Pieces caught){
		switch (caught.getColor()) {
		case White:
			whitePlayer.remove(caught);
			break;
		default:
			blackPlayer.remove(caught);
			break;
		}
		
		
	}
	
	
	public static boolean isEnd() {
		return end;
	}
	
		private static boolean trialMove(int row1, int col1, int row2, int col2, Board board, PieceColor color) 
				throws Exception,IllegalMoveException{
			PieceColor opposite;
			switch (color) {
			case White:
				opposite = PieceColor.Black;
				break;
			default:
				opposite = PieceColor.White;
				break;
			}
			isPathBlocked(board, row1, col1, row2, col2);
			Square start = board.getSquare(row1, col1);
			Square dest = board.getSquare(row2, col2);
			Pieces player = start.getPiece();
			boolean temp = player.hasMoved();
			Pieces p2Temp = dest.getPiece();
			boolean hasMovedTemp2 = false;
			if(p2Temp == null) {
			} else {
				hasMovedTemp2 = p2Temp.hasMoved();
			}
			
			switch (player.getPieceType()) {
			case KING:
				Pieces rook_castle;
				switch (player.getColor()) {
				case White:
					if (start.toString().equals("[7,4]") && (dest.toString().equals("[7,6]") || dest.toString().equals("[7,2]"))) {
						if (!dest.toString().equals("[7,6]")) {
							switch (dest.toString()) {
							case "[7,2]":
								if (!board.getSquare(dest.getRow(), dest.column() + 1).isEmpty()) {
									throw new Exception();
								}
								rook_castle = board.getSquare(dest.getRow(), dest.column() - 2).getPiece();
								if (checkCastle(player, rook_castle, board)) {
									return true;
								}	
								else {
									throw new IllegalMoveException();
								}
							}
						} else {
							if (board.getSquare(dest.getRow(), dest.column() - 1).isEmpty()) {
							} else {
								throw new Exception();
							}
							rook_castle = board.getSquare(dest.getRow(), dest.column() + 1).getPiece();
							if (!checkCastle(player, rook_castle, board)) {
								throw new IllegalMoveException();
							} else {
								return true;
							}
						}
					}
					break;
				case Black:
				default:
					if (start.toString().equals("[0,4]") && (dest.toString().equals("[0,6]") || dest.toString().equals("[0,2]"))) {
						switch (dest.toString()) {
						case "[0,6]":
							if (board.getSquare(dest.getRow(), dest.column() - 1).isEmpty()) {
							} else {
								throw new Exception();
							}
							rook_castle = board.getSquare(dest.getRow(), dest.column() + 1).getPiece();
							if (!checkCastle(player, rook_castle, board)) {
								throw new IllegalMoveException();
							} else {
								return true;
							}
						}
						switch (dest.toString()) {
						case "[0,2]":
							if (!board.getSquare(dest.getRow(), dest.column() + 1).isEmpty()) {
								throw new Exception();
							}
							rook_castle = board.getSquare(dest.getRow(), dest.column() - 2).getPiece();
							if (checkCastle(player, rook_castle, board)) {
								return true;
							}		
							else {
								throw new IllegalMoveException();
							}
						}	
					}
					break;
				}
				break;
			case BISHOP:
				break;
			case KNIGHT:
				break;
			case PAWN:
				break;
			case QUEEN:
				break;
			case ROOK:
				break;
			default:
				break;
			}
			switch (player.getPieceType()) {
			case PAWN:
				//check for pawns ability to move diagonal for capture
				if (!pawnCapture(player, board, dest)) {
					if (player.getColor() != PieceColor.White) {
						if (dest != board.getSquare(start.getRow() + 1, start.column()) && player.hasMoved()) {
							if (!player.hasMoved()) {
								if (dest != board.getSquare(start.getRow() + 2, start.column()) && (dest.isEmpty() ||
										dest.getPiece().getColor() == PieceColor.Black)) {
									throw new IllegalMoveException();
								}
							} else {
								if ((pawnCapture(player, board, dest) || !dest.isEmpty()) && !dest.isEmpty() && dest.getPiece().getColor() != PieceColor.Black) {
								} else {
throw new IllegalMoveException();
}
							}
						}
						if (dest == board.getSquare(start.getRow() + 1, start.column())  || dest == board.getSquare(start.getRow() + 2, dest.column())) {
							Pieces blocked = dest.getPiece();
							if (blocked == null) {
							} else {
								throw new Exception();
							}
						}
					} else {
						if (dest != board.getSquare(start.getRow() - 1, start.column())) {
							if (!player.hasMoved()) {
								if (dest == board.getSquare(start.getRow() - 2, start.column()) || (!dest.isEmpty() && dest.getPiece().getColor() != PieceColor.White)) {
								} else {
throw new IllegalArgumentException();
}
							} else { 
								if ((pawnCapture(player, board, dest) || !dest.isEmpty()) && !dest.isEmpty() && dest.getPiece().getColor() != PieceColor.White) {
								} else {
throw new IllegalMoveException();
}
							}
						}
						if (dest == board.getSquare(start.getRow() - 1, start.column())  || dest == board.getSquare(start.getRow() - 2, dest.column())) {
							Pieces blocked = dest.getPiece();
							if (blocked == null) {
							} else {
								throw new Exception();
							}
						}
					}
				} else {
					Square adj_square;
					switch (player.getColor()) {
					case White:
						adj_square = board.getSquare(dest.getRow() + 1, dest.column());
						board.getBlackCounts().put(PieceType.PAWN, board.getBlackCounts().get(PieceType.PAWN) - 1);
						break;
					default:
						adj_square = board.getSquare(dest.getRow() - 1, dest.column());
						board.getWhiteCounts().put(PieceType.PAWN, board.getWhiteCounts().get(PieceType.PAWN) - 1);
						break;
					}
					adj_square.removePiece();
				}
				break;
			case BISHOP:
				break;
			case KING:
				break;
			case KNIGHT:
				break;
			case QUEEN:
				break;
			case ROOK:
				break;
			default:
				break;
			}
			player.move(dest.getRow(), dest.column());
			updateProtectedSquares(board);
			player.setMoved();
			dest.occupy(player);
			start.removePiece();
			switch (dest.getPiece().getPieceType()) {
			case PAWN:
				if (dest.getPiece().getColor() == PieceColor.White) {
					if (dest.getRow() == 0) {
						dest.removePiece();
						dest.occupy(new Queen(dest.getRow(), dest.column(), color));
						board.getWhiteCounts().put(PieceType.QUEEN, board.getWhiteCounts().get(PieceType.QUEEN) + 1);
						board.getWhiteCounts().put(PieceType.PAWN, board.getWhiteCounts().get(PieceType.PAWN) - 1);
					}
				}
				else {
					if (dest.getRow() == 7) {
						dest.removePiece();
						dest.occupy(new Queen(dest.getRow(), dest.column(), color));
						board.getBlackCounts().put(PieceType.QUEEN, board.getBlackCounts().get(PieceType.QUEEN) + 1);
						board.getBlackCounts().put(PieceType.PAWN, board.getBlackCounts().get(PieceType.PAWN) - 1);
					}
				}
				break;
			case BISHOP:
				break;
			case KING:
				break;
			case KNIGHT:
				break;
			case QUEEN:
				break;
			case ROOK:
				break;
			default:
				break;
			}
			updateProtectedSquares(board);
			inCheck(board, opposite);
			player.actualMove(row1, col1, temp);
			start.occupy(player);
			dest.removePiece();
			if(p2Temp != null){
				p2Temp.actualMove(row2, col2, hasMovedTemp2);
				dest.occupy(p2Temp);		
			}
			if(check) {
			} else {
				return true;
			}
			return false;
		}

	public static boolean getCheckStatus() {
		return check;
	}
	
	private static boolean pawnCapture(Pieces piece1, Board currentBoard, Square currentSquare) throws Exception, IllegalMoveException {
		switch (piece1.getPieceType()) {
		case PAWN:
			Square pawnPos = currentBoard.getSquare(piece1.getRow(), piece1.getCol());
			Pieces nieghborPawn;
			Square neighbor;
			if (piece1.getColor() != PieceColor.White) {
				if (pawnPos.getRow() == 4) {
				} else {
					return false;
				}
				neighbor = currentBoard.getSquare(currentSquare.getRow() - 1, currentSquare.column());
				if (!neighbor.isEmpty()) {
				} else {
					return false;
				}
				nieghborPawn = neighbor.getPiece();
				if (nieghborPawn.getPieceType() == PieceType.PAWN) {
				} else {
					return false;
				}
				if (!white_moves.isEmpty()) {
					int[] white_last_move = GameMoves.convertMove(white_moves.get(white_moves.size() - 1)); //retrieve last move white made
					//last move white made has to be a 2-square pawn move in an adjacent row
					if (white_last_move[0] == 6 && white_last_move[1] == currentSquare.column() && white_last_move[2] == currentSquare.getRow() - 1
							&& white_last_move[3] == currentSquare.column()) {
										} else {
return false;
}//end if white 
				}//end if white empty 			
				else {
					return false;
				}
			}//end if piece=white 
			else {
				if (pawnPos.getRow() == 3) {
				} else {
					return false;
				}
				neighbor = currentBoard.getSquare(currentSquare.getRow() + 1, currentSquare.column());
				if (!neighbor.isEmpty()) {
				} else {
					return false;
				}
				nieghborPawn = neighbor.getPiece();
				if (nieghborPawn.getPieceType() == PieceType.PAWN) {
				} else {
					return false;
				}
				if (!black_moves.isEmpty()) {
					int[] move =  GameMoves.convertMove(black_moves.get(black_moves.size() - 1)); //retrieve last move black made
					//Blacks last move must meet criteria 
					if (move[0] == 1 && move[1] == currentSquare.column() && move[2] == currentSquare.getRow() + 1
							&& move[3] == currentSquare.column()) {
										} else {
return false;
}//end black last move 
				}//end black move empty 		
				else {
					return false;
				}
			}//end if piece white 
			break;
		default:
			return false;
		}
		return true;
	}
	
	
	
	
}


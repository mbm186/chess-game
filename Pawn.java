package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */



public class Pawn extends Pieces {
	//the color, row and column are needed and we must call super because of the constructor rule
	public Pawn(int row, int col, PieceColor color){
		super(row, col, PieceType.PAWN, color);
	}
   //checks the move of a pawn and assumes it can move diagonal 
	@Override
	public void move(int row, int column) throws IllegalMoveException {
		super.movePiece(row, column, (r, c) -> {
		switch (getColor()) {
		case Black:
			if(r < getRow()){
				return false;
			}
			if(r == getRow() + 1 && Math.abs(c - getCol()) <= 1){
				return true;
			}
			if(!hasMoved() && r == getRow() + 2 && c == getCol()){
				return true;
			}
			return false;
		default:
			if(r > getRow()){
				return false;
			}
			if(r == getRow() - 1 && Math.abs(c - getCol()) <= 1){
				return true;
			}
			if(!hasMoved() && r == getRow() - 2 && c == getCol()){
				return true;
			}
			return false;
		}
	});
}
	// this returns the string of Pawn players
	public String toString() {
		char colorCharr;
		if (getColor() == PieceColor.White) {
			colorCharr = 'w';		}
		else {			colorCharr = 'b';		}
		return colorCharr + "P";
	}
}

package game;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
 
import gamePieces.Pieces;

public class Square {
	
	private char file;  //char rep of row 
	private int row;
	private int column;
	private int color; 
	private boolean squareEmpty;
	private Pieces piece;
	
	public Square(char rowFile, int temprowr, int tempcolumn, int tempColor) {
		this.row = temprowr;
		this.file = rowFile;
		this.column = tempcolumn;
		this.color = tempColor;
		this.squareEmpty = true;
		this.piece = null;
	}
	
	public boolean isEmpty() {
		return squareEmpty;
	}
	
	
	public int getColor() {
		return color;
	}
	
	public int getRow() {
		return row;
	}
	
	public Pieces getPiece() {
		return piece;
	}
	
	public int column() {
		return column;
	}
	
	public void removePiece() {
		piece = null;
		squareEmpty = true;
	}
	
	public char getCharRow() {
		return file;
	}
	
	public void occupy(Pieces p) {
		piece = p;
		squareEmpty = false;
	}
	
	public String display() {
		if (squareEmpty) {
			switch (color) {
			case 0:
				return "##";
			}
			return "  ";
		}
		return getPiece().toString();
	}
	
	public String getNotation() {
		return file + "" + (8 - row);
	}
	
	public String toString() {
		return "[" + row + "," + column + "]";
	}
		
}


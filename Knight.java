package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
public class Knight extends Pieces{
	//the color, row and column are needed and we must call super because of the constructor rule
	public Knight(int row, int col, PieceColor color){
		super(row, col, PieceType.KNIGHT, color);
	}
	//calls bifunction in pieces
	@Override
	public void move(int row, int column) throws IllegalMoveException {
		// TODO Auto-generated method stub
		// knights pattern check
		super.movePiece(row, column, (r, c) -> {
			if((Math.abs(r - getRow()) == 2 && Math.abs(c - getCol()) == 1) || 
					(Math.abs(r - getRow()) == 1 && Math.abs(c - getCol()) == 2)){
				return true;
			}
			return false;
		});
	}
	//calls bifunction in pieces
	public void isMoveLegal(int row, int column) throws IllegalMoveException {
		super.isMoveLegal(row, column); 
		if ((Math.abs(row - getRow()) == 2 && Math.abs(column - getCol()) == 1) || 
				(Math.abs(row - getRow()) == 1 && Math.abs(column - getCol()) == 2))
			return;
		throw new IllegalMoveException();
	}
	// this returns the string of Knight players
	public String toString() {
		char colorCharr;
		if (getColor() == PieceColor.White) {
			colorCharr = 'w';		}
		else {			colorCharr = 'b';		}
		return colorCharr + "N";
	}
	
}
	

package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */

public class Bishop extends Pieces {
	//the row, column and color are needed, this is the design of our pieces, these 3 are needed in the constructors 
	public Bishop(int row, int col, PieceColor color){
		super(row, col, PieceType.BISHOP, color);
	}
	//here we are setting the rules for how the bishop moves to check if it is legal. If it is legal, then we will continue
	//otherwise we need to throw an illegal exception 
	public void isMoveLegal(int row, int column) throws IllegalMoveException {
		super.isMoveLegal(row, column);
		int dr = row - getRow();
		if (column == getCol() + dr || column == getCol() - dr)
			return;
		throw new IllegalMoveException();
	}
	//calls bifunction in pieces
	@Override
	public void move(int row, int column) throws IllegalMoveException {
		// TODO Auto-generated method stub
		super.movePiece(row, column, (r, c) -> {
			int dr = row - getRow();
			return (column == getCol() + dr || column == getCol() - dr);
		});
	}
	//returns string representation of a bishop
		public String toString() {
			char color_letter;
			switch (getColor()) {
			case White:
				color_letter = 'w';
				break;
			default:
				color_letter = 'b';
				break;
			}
			return color_letter + "B";
		}
}


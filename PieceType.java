package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */

public enum PieceType{
	
	
	 KING , QUEEN , ROOK, BISHOP, KNIGHT, PAWN ;

}

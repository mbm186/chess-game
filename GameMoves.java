package chess;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import game.TestBoard;
import gamePieces.IllegalMoveException;

public class GameMoves extends Chess {
	static int[] result = new int[4];
	public static int[] convertMove(String temp) throws Exception{
		if (resign && temp.equalsIgnoreCase("draw")) {
			TestBoard.setEnd();
			System.out.println("Draw.");
		}
		switch (temp.length()) {
		case 11:
			String quit = temp.substring(6);
			switch (quit) {
			case "draw?":
				resign = true;
				break;
			}
			break;
		default:
			if(temp.length() != 5){
				//System.out.println("here less than 5 ");
				//throw new IllegalMoveException();
			}
			break;
		} 
		for(String s: temp.split(" ")){
			if(s.length() == 2 || s.equals("draw?")) {
			} else {
				//System.out.println("here less than 2 ");
				//throw new IllegalMoveException();
			}
		}
		//sets pattern in for matching 
		String regex = "[a-h][1-8]";
		//compiles the squars in a pattern 
		Pattern r = Pattern.compile(regex);
		Matcher m = r.matcher(temp);
		for(int i = 0; i <= 2; i+=2) {
			if(!m.find()) {
				throw new Exception();
			} else {
				String output = m.group(0);
				//finds output saves as int[]
				result[i] = 8 - Integer.parseInt(output.charAt(1) + "");
				result[i + 1] = output.charAt(0) - 'a';
			}
		}// returns int [] of the move to check path 
		return result;
	}	
}

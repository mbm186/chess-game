package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */


public class King extends Pieces{
	//the color, row and column are needed and we must call super because of the constructor rule
	public King(int row, int column, PieceColor color){
		super(row, column, PieceType.KING, color);
	}
	
    //here we are setting the rules for how the king moves to check if it is legal. If it is legal, then we will continue
	//otherwise we need to throw an illegal exception
	public void isMoveLegal(int row, int column) throws IllegalMoveException {
		super.isMoveLegal(row, column);
		if (row >= getRow() - 1 && row <= getRow() + 1 && column >= getCol() - 1
				&& column <= getCol() + 1)
			return;
		throw new IllegalMoveException();
	}

	//calls bifunction in pieces
	@Override
	public void move(int krow, int column) throws IllegalMoveException {
		super.movePiece(krow, column, (r, c) -> {
			if (r != getRow() || (c != getCol() + 2 && c != getCol() - 2)) {
				if(r < getRow() - 1 || r > getRow() + 1 || c < getCol() - 1 || c > getCol() + 1){
					return false;
				}
			} else {
				if (hasMoved()) {
					return false;
				}
				return true;
			}
			return true;
		});
		
	}
// this returns the string of king players	
	public String toString() {
		char ColorChar;
		if (getColor() == PieceColor.White) {
			ColorChar = 'w';
		}		else {
			ColorChar = 'b';
		}
		return ColorChar + "K";
	}
}



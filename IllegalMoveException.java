package gamePieces;

/**
 * 
 * 
 * @author Jiya Kohli and Mohammad Memon
 * 
 */
public class IllegalMoveException extends Exception {	
	
	//class for illegal moves 
	
public IllegalMoveException(){
	super();
}


public IllegalMoveException(String msg){
	
	super(msg);
}

}
